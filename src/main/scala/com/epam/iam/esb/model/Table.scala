package com.epam.iam.esb.model

trait Table {
  def tableName: String
  def generateSelectAllSqlQuery:String = s"select * from $tableName"
}
