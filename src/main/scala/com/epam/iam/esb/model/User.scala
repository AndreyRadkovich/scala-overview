package com.epam.iam.esb.model

case class User(login: String, password: String = "", typeId: Long = 1) extends Table with Immutable {
  override def tableName: String = "USER"
}

case class Role(typeId: Long, description: String) extends Table with Immutable {
  override def tableName: String = "ROLE"
}
