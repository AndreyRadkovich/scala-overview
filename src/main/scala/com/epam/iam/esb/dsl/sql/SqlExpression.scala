package com.epam.iam.esb.dsl.sql

trait SqlExpression {
  def exec(): Unit
}

class SelectExp extends SqlExpression {
  override def exec(): Unit = {}
}

class UpdateExp extends SqlExpression {
  override def exec(): Unit = {}
}
