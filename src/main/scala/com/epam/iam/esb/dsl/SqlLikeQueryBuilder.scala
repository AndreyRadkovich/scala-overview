package com.epam.iam.esb.dsl

class SqlLikeQueryBuilder {
  def from[A](table: List[A]): From[A] = new From[A](table = table)
}

class From[A](table: List[A]) {
  def join[B](anotherTable: List[B]): Join[A, B] = new Join[A, B](table, anotherTable)
}

class Join[A, B](table: List[A], anotherTable: List[B]) {
  def on(op: (A, B) => Boolean): On[A, B] = new On[A, B](table, anotherTable, op)
}

class On[A, B](table: List[A], anotherTable: List[B], op: (A, B) => Boolean) {
  def where(condition: (A, B) => Boolean): Where[A, B] = {
    new Where[A, B](table, anotherTable, (a, b) => op(a, b) && condition(a, b))
  }
}

class Where[A, B](table: List[A], anotherTable: List[B], condition: (A, B) => Boolean) {
  def select[C](op: (A, B) => C) = {
    for (row <- table; anotherRow <- anotherTable; if condition(row, anotherRow)) yield {
      op(row, anotherRow)
    }
  }
}
