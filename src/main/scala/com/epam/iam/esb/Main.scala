package com.epam.iam.esb

import com.epam.iam.esb.dsl.SqlLikeQueryBuilder
import com.epam.iam.esb.model.{Role, User}

object Main {

  def main(args: Array[String]): Unit = {
    val users = List[User](User("login1"), User("login2"), User("login3", "password", 2))
    val types = List[Role](Role(1, "CORPORATE"), Role(2, "ADMIN"))

    val tuples = new SqlLikeQueryBuilder()
      .from(users).join(types)
        .on((user, role) => user.typeId == role.typeId)
        .where((user, role) => user.typeId == 1)
      .select((user, role) => (user.login, role.description))
    tuples.foreach(println)
  }
}
